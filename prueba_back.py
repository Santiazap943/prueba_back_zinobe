import abc
import requests
import numpy as np
import pandas as pd
import json
import hashlib
import random
import sqlite3
import unittest
from time import time

class Contrie():
    def __init__(self):
        super().__init__()
        self.url = "https://restcountries-v1.p.rapidapi.com/all"
    def getCountries(self):
        headers = {
            'x-rapidapi-key': "ec8bf0c851mshc4763e393d64adfp19b329jsn8722e7a7ab3c",
            'x-rapidapi-host': "restcountries-v1.p.rapidapi.com"
            }
        return requests.request("GET", self.url, headers=headers).json()
    def getRegion(self):
        regiones=[]
        for i in self.getCountries():
            if i['region'] not in regiones and i['region'] != "":
                regiones.append(i['region']) 
        return regiones

class City():
    def __init__(self):
            super().__init__()
            self.url = "https://restcountries.eu/rest/v2/region/"
    def getCity(self, region):
        consulta = self.url + region
        if region != "":
            req = requests.request("GET", consulta).json()
            request =req[random.randint(0,len(req)-1)]
            return request
        else:
            return

class Language():
    def __init__(self):
        super().__init__()
        self.url = "https://restcountries.eu/rest/v2/lang/"
    def getLanguage(self,cod):
        if cod != "":
            consulta = self.url + cod
            req = requests.request("GET", consulta).json()
            lang = hashlib.sha1(b""+req[0]["name"].encode()).hexdigest()
            return lang
        else:
            return

class Connection():
    def open(self):
        try:
            con = sqlite3.connect('prueba_back.db')
            return con
        except Error:
            print(Error)
    def createTable(self,con):
        c = con.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS infoRegion(id integer PRIMARY KEY AUTOINCREMENT , Region text, CityName text, Language text, Time text)")
        con.commit()
        c.execute("CREATE TABLE IF NOT EXISTS estadistica(id integer PRIMARY KEY AUTOINCREMENT , Total text, Promedio text, Minimo text, Maximo text)")
        con.commit()
    def insertInfoRegion(self,con,data):
        c = con.cursor()
        c.execute("INSERT INTO infoRegion(Region, CityName, Language, Time) VALUES('"+data["region"]+"', '"+data["cityName"]+"', '"+data["language"]+"', '"+data["time"]+"')")
        con.commit()
    def insertEstadistica(self,con,data):
        c = con.cursor()
        c.execute("INSERT INTO estadistica(Total, Promedio, Minimo, Maximo) VALUES('"+data["Total"]+"', '"+data["Promedio"]+"', '"+data["Minimo"]+"', '"+data["Maximo"]+"')")
        con.commit()
    def selectAllInfoRegion(self,con):
        c = con.cursor()
        c.execute('SELECT * FROM infoRegion')
        return c.fetchall()
    def selectAllEstadistica(self,con):
        c = con.cursor()
        c.execute('SELECT * FROM estadistica')
        return c.fetchall()
    def closeConnection(self,con):
        con.close()

class Test(unittest.TestCase):
    def testGetCountries(self):
        co = Contrie()
        self.assertIsNotNone(co.getCountries())
    def testGetRegion(self):
        co = Contrie()
        self.assertIsNotNone(co.getRegion())
        self.assertEqual(len(co.getRegion()),6)
    def testGetCity(self):
        ci = City()
        rg = Contrie()
        for i in regiones:
            self.assertIsNotNone(i)
        self.assertIsNone(ci.getCity(""))
    def testGetLanguaje(self):
        la = Language()
        ci = City()
        rg = Contrie()
        for i in regiones:
            c = ct.getCity(i)
            self.assertIsNotNone(la.getLanguage(c["languages"][0]["iso639_1"]))
        self.assertIsNone(la.getLanguage(""))
    def testOpen(self):
        connection = Connection()
        con = connection.open()
        self.assertIsNotNone(con)
        connection.closeConnection(con)

connection = Connection()
con = connection.open()
connection.createTable(con)
rg = Contrie()
ct = City()
lg = Language()
city = []
lang = []
tiempo = []
regiones = rg.getRegion()
for i in regiones:
    t0=time()
    c = ct.getCity(i)
    city.append(c["name"])
    l =lg.getLanguage(c["languages"][0]["iso639_1"])
    lang.append(l)
    t1=time()
    tiempo.append(t1-t0)
    connection.insertInfoRegion(con,{"region":i,"cityName":c["name"],"language":l,"time":str(t1-t0)})

dic = {"Region":regiones,"CityName":city,"Language":lang,"Time":tiempo}
df1=pd.DataFrame(dic)
tiempos = {"Total":str(df1["Time"].sum()), "Promedio":str(df1["Time"].mean()), "Minimo":str(df1["Time"].min()), "Maximo":str(df1["Time"].max())}
connection.insertEstadistica(con,tiempos)

jsonOutput = df1.to_json()
with open('data.json', 'w') as file:
    json.dump(jsonOutput, file)  

print("<-----------------DataFrame----------------->")       
print(df1)
print("<-----------------Estadisticas----------------->")
print("Tiempo total: " +tiempos["Total"])
print("Tiempo promedio: " +tiempos["Promedio"])
print("Tiempo minimo: " +tiempos["Minimo"])
print("Tiempo maximo: " +tiempos["Maximo"])
print("<-----------------Estado actual de la base de datos----------------->")
for i in connection.selectAllInfoRegion(con):
    print(i)
for i in connection.selectAllEstadistica(con):
    print(i)
print("<-----------------Archivo json----------------->")
print(jsonOutput) 
connection.closeConnection(con)

if __name__ == "__main__":
    unittest.main()